#!/usr/bin/env python3.12

import os
import sys
import signal
import difflib
from subprocess import Popen, PIPE

def signal_handler(sig, frame):
    sys.stdout.write('\033[?25h')
    sys.stdout.write('\033[0m')
    sys.exit(0)

def read_file(filename):
    with open(filename, 'r') as file:
        return file.readlines()

def compare_files(file1, file2):
    lines1 = read_file(file1)
    lines2 = read_file(file2)

    diff = difflib.unified_diff(lines1, lines2, file1, file2, lineterm='')
    return list(diff)

def colored_diff(diff):
    colored_diff_lines = []
    for line in diff:
        if line.startswith(' '):
            colored_diff_lines.append('\033[32m' + line + '\033[0m')  # Green
        elif line.startswith('-'):
            colored_diff_lines.append('\033[31m' + line + '\033[0m')  # Red
        elif line.startswith('+'):
            colored_diff_lines.append('\033[33m' + line + '\033[0m')  # orange
        else:
            colored_diff_lines.append(line)
    return colored_diff_lines

def main():
    if len(sys.argv) < 3:
        print("Usage: ./versioning.py file1 file2 [file3 ...]")
        print("Usage: ./versioning.py old actual")
        sys.exit(1)

    signal.signal(signal.SIGINT, signal_handler)
    try:
        for i in range(1, len(sys.argv) - 1):
            file1 = sys.argv[i]
            file2 = sys.argv[i + 1]
            diff = compare_files(file1, file2)
            colored_diff_lines = colored_diff(diff)
            diff_output = "".join(colored_diff_lines)

            process = Popen(['less', '-R'], stdin=PIPE, env=dict(os.environ, LESS='FRX'))
            process.communicate(input=diff_output.encode('utf-8'))

            print("=" * 80)

    except KeyboardInterrupt:
#        if process:
        process.kill()
        sys.exit()

if __name__ == "__main__":
    main()
